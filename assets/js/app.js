/* global angular */

var app = angular.module('webapp', ['ngRoute', 'ngSanitize', 'ngAnimate', 'angular-svg-round-progressbar']);

app.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/playlist', {
            controller: 'PlaylistCtrl',
            controllerAs: 'playlistCtrl',
            templateUrl: 'partials/playlist.html'
        })
        .otherwise({ redirectTo: '/playlist' });

}]);


app.filter('extLowerCase', [function() {
    return function(f) {
        if (!f) return false;
        return f.split('.').pop().toLowerCase();
    };
}]);