// ----------------------------------------------------------------------------
// Variables
// ----------------------------------------------------------------------------
var status = {
    // Config de la webapp utilisée
    config: null,
    // Est connecté au socket distant
    connected: false,
    // Est authentifié au socket distant
    authenticated: false,
    // Est loggé au socket distant
    logged: false
};

var systemInfos = {};
var settings = {};

var playlist = {};

var serverURL = 'https://server.inovbox.fr:3000/';
var port = 3000;
var localUrl = 'http://localhost:'+port;

// ----------------------------------------------------------------------------
// Requires
// ----------------------------------------------------------------------------
// ExpressJS
var express = require('express');
var app = express();

// Serveur HTTP NodeJS
var http = require('http').createServer(app);
var request = require('request');

var os = require('os');
var fs = require('fs');
var log = require(__dirname + '/log.js');

// permet de questionner l'user
var prompt = require('prompt');

// pour remplacer/écrire dans un fichier
var replace = require('replace-in-file');

// OMX Manager
var OmxManager = require('omx-manager');
var manager = new OmxManager();
var cameraInstance;

// Socket interne (NodeJS <=> Angular)
var server = require('socket.io')(http);

// Socket distant
var client = require('socket.io-client')(serverURL, {
    autoConnect: false
});

function startApp() {
    // on lance le serveur express
    log.debug('initServer');
    initServer();

    // on lance le socket
    log.debug('initSocket');
    initSocket();
}

// ----------------------------------------------------------------------------
// Gestion de la config et des settings
// ----------------------------------------------------------------------------
function readConfigFile() {
    log.debug("Reading config file...");

    if (fs.existsSync('assets/data/config.json')) {
        fs.readFile('assets/data/config.json', function(err, data) {
            if (!err) {
                // on se connecte
                status.config = JSON.parse(data);
                log.info(status.config);
                startApp();
            } else {
                log.error("Unable to read config file!");
            }
        });
    } else {
        prompt.start();
        prompt.get(['serial', 'passkey'], function (err, result) {

            status.config = {
                serial: result.serial,
                passkey: result.passkey
            };

            fs.writeFile('assets/data/config.json', JSON.stringify(status.config, null, 4), 'utf8', function() {
                log.success('Config file saved.');
                log.debug(status.config);
                startApp();
            });
        });
    }
};

function saveConfigFile(data) {
    status.config = data;
    fs.writeFile('assets/data/config.json', JSON.stringify(status.config, null, 4), 'utf8', function() {
        log.info("Config file saved.");
        server.emit('config:saved', { message: "Config file saved." });

        if (!status.logged) {
            client.emit('login', status.config);
        }
    });
};

function saveDisplayRotation(rotation) {
    if (systemInfos.platform == 'linux') {
        // sauvegarde rotation
        var options = {
            files: '/boot/config.txt',
            from: /display_rotate=[0-9]+/g,
            to: 'display_rotate=' + rotation,
            allowEmptyPaths: true
        };

        replace(options)
            .then(function(changedFiles) {
                log.info("Display rotation saved");
            })
            .catch(function(error) {
                log.error("Display rotation not saved");
            });
    }
}

// ----------------------------------------------------------------------------
// Gestion de la playlist
// ----------------------------------------------------------------------------
function readPlaylistFile() {
    log.debug("Reading playlist file...");

    fs.readFile('assets/data/playlist.json', function(err, data) {
        if (!err) {
            // on se connecte
            playlist = JSON.parse(data);
            log.debug("Playlist read.");
            server.emit('playlist:sync', playlist);
        }
        else {
            log.error("Playlist file not found!");
            log.info("Requesting playlist");
            client.emit('playlist:get', status);
        }
    });
};

function savePlaylistFile() {
    fs.writeFile('assets/data/playlist.json', JSON.stringify(playlist, null, 4), 'utf8', function() {
        log.info("Playlist file saved.");
        server.emit('playlist:saved', { message: "Playlist file saved." });
        server.emit('playlist:sync', playlist);
    });
};

var download = function(element){
    var filename = 'https://api.inovbox.fr/upload/' + element.filename;
    return new Promise(function(resolve, reject) {
        request.head(filename, function(err, res, body){
            var filePath = __dirname+"/upload/"+element.filename;
            if(!err) {
                request(filename).pipe(fs.createWriteStream(filePath)).on('close', function() {
                    log.success('Download "'+filename+'" completed');
                    resolve();
                });
            }
            else {
                reject(err);
            }
            //console.log('content-type:', res.headers['content-type']);
            //console.log('content-length:', res.headers['content-length']);
        });
    });
};

// ----------------------------------------------------------------------------
// Gestion du socket interne
// NodeJS <=> Angular
// ----------------------------------------------------------------------------
server.on('connection', function(socket) {

    log.info("Webapp socket connected.");
    socket.emit('status', status);
    readPlaylistFile();

    // on envoi les infos systèmes
    socket.emit('system:infos', systemInfos);

    // on envoi les settings
    socket.emit('device:settings', settings);

    socket.on('config:save', function(data) {
        log.info("Config received!");
        saveConfigFile(data);
    });

    socket.on('video:play', function(filename) {
        log.debug("Play video (via OMX): " + filename);
        cameraInstance = manager.create('/home/pi/Inovbox/inovbox-public-client/upload/' + filename);
        cameraInstance.play();
    });

    socket.on('video:stop', function() {
        log.debug("Stop video");
        try {
            cameraInstance.stop();
        } catch (err) {
            log.warn("Video not playing");
        }
    });

    socket.on('disconnect', function() {
        log.warn("Webapp socket disconnected.");
    });
});

// ----------------------------------------------------------------------------
// Gestion du socket distant
// Serveur <=> Webapp
// ----------------------------------------------------------------------------
function initSocket() {
    client.connect();

    client.on('connect', function() {
        log.info("Connected.");
        status.connected = true;
        server.emit('status', status);
        client.emit('login', status.config);
    });

    client.on('logged', function(device) {
        log.success("Logged successfully.");
        status.logged = true;
        status.authenticated = true;
        server.emit('status', status);

        log.info(device);
        settings = device;
        server.emit('device:settings', settings);
        saveDisplayRotation(settings.rotation);
    });

    client.on('err', function(data) {
        log.error(data.type + ": " + data.message);
        status.authenticated = false;
    });

    client.on('disconnect', function() {
        log.warn("Disconnected");
        status.connected = false;
        status.authenticated = false;
        status.logged = false;
        server.emit('status', status);
    });

    client.on('playlist:send', function(data) {
        log.debug('Playlist received');
        playlistSync(data);
    });

    client.on('playlist:sync', function(data) {
        log.debug('Playlist sync');
        playlistSync(data);
    });

    client.on('device:settings', function(data) {
        log.debug('Settings received');
        log.info(data);
        settings = data;
        server.emit('device:settings', settings);
        saveDisplayRotation(data.rotation);
    });

    client.on('device:reboot', function(data) {
        log.debug('Rebooting...');

        // Require child_process
        var exec = require('child_process').exec;

        // Create shutdown function
        function shutdown(callback){
            exec('shutdown -r now', function(error, stdout, stderr){ callback(stdout); });
        }

        // Reboot computer
        shutdown(function(output){
            log.info("Reboot");
        });
    });
}

function playlistSync(data) {
    log.info("Playlist synchronisation");
    // on avertit la webapp
    server.emit('playlist:received', playlist);
    // Recevoir la playlist
    playlist = data;
    log.info(playlist);
    log.info("Downloading assets...");

    //Télécharger avec node les fichiers
    var downloads = playlist.elements.map(download);
    var results = Promise.all(downloads);
    results.then(function() {
        //Ajout du chemin local des fichiers
        playlist.elements.forEach(function(element) {
            element.filePath = localUrl+"/upload/"+element.filename;
        });
        savePlaylistFile();
    });
}

// ----------------------------------------------------------------------------
// Router/Express
// ----------------------------------------------------------------------------
// On définit les assets statiques (CSS, JS, etc.)
app.use(express.static('assets'));
app.use('/upload', express.static('upload'));

// On définit une route par défaut
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/assets/partials/index.html');
    log.info('GET "/"');
});

function initServer() {
    // On ouvre le serveur sur le port 3000
    http.listen(3000, function() {
        log.info('Listening on *:3000');
    });
}

// ----------------------------------------------------------------------------
// Router/Express
// ----------------------------------------------------------------------------
systemInfos = {
    platform: os.platform(),
    networkInterfaces: os.networkInterfaces()
}

readConfigFile();